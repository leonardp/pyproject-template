{
  description = "Python project example";

  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    systems.url = "github:nix-systems/x86_64-linux";

    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix.url = "github:nix-community/poetry2nix";
    poetry2nix.inputs.nixpkgs.follows = "nixpkgs";

    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
    pre-commit-hooks-nix.inputs.nixpkgs.follows = "nixpkgs";

  };

  outputs = inputs@{ flake-parts, poetry2nix, devenv, ... }: flake-parts.lib.mkFlake { inherit inputs ; } {
    systems = import inputs.systems;
    imports = [ devenv.flakeModule ];

    perSystem = { config, self', inputs', pkgs, system, ... }: {
      _module.args.pkgs = import inputs.nixpkgs { inherit system; overlays = [ inputs.poetry2nix.overlays.default ]; };

      # package --> nix build
      packages.trogon = pkgs.poetry2nix.mkPoetryApplication {
        projectDir = pkgs.fetchFromGitHub {
          owner = "Textualize";
          repo = "trogon";
          rev = "083e95c76227543949c1c48bcdfacae5a37e09bd";
          hash = "sha256-5cHYBomG9qXVR5Lfq48aGsib3I9xpeKUsJ15jkvVrrM=";
        };
        #preferWheel = true;
        overrides = pkgs.poetry2nix.overrides.withDefaults (final: prev: {
          # Notice that using .overridePythonAttrs or .overrideAttrs wont work!
          mypy = prev.mypy.override {
            preferWheel = true;
          };
        });
      };

      #packages.pyimgui = pkgs.callPackage ./nix/pyimgui.nix {};
      #packages.dearpygui = pkgs.callPackage ./nix/dearpygui.nix { buildPythonPackage=pkgs.python3Packages.buildPythonPackage;};

      packages.default = pkgs.poetry2nix.mkPoetryApplication {
        projectDir = ./.;
        overrides = pkgs.poetry2nix.overrides.withDefaults (final: prev: {
          #dearpygui = config.packages.dearpygui;
          trogon = config.packages.trogon;
        });

        buildInputs = with pkgs.python3Packages; [ pkgs.sphinxHook furo ];
        sphinxBuilders = [ "html" "man" ];
        checkPhase = ''
          runHook preCheck
          echo $(date) > docs/rundate.txt
          pytest tests -vrA > docs/pytest.txt
          sed -i 's/\/nix\/store\/[[:alnum:]]*-//' docs/pytest.txt
          runHook postCheck
        '';
      };

      # devshell --> nix develop --impure
      devenv.shells.default = {
        name = "devshell";
        containers = pkgs.lib.mkForce {}; # workaround to disable containers
        pre-commit.hooks = {
          black.enable = true;
          isort.enable = true;
          #mypy.enable = true;
        };
        difftastic.enable = true;
        languages.python = {
          enable = true;
          poetry = {
            enable = true;
            activate.enable = true;
            install = {
              enable = true;
              installRootPackage = true;
            };
          };
        };
        env.GREETING = "hellö";
        env.LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib}/lib/:/run/opengl-driver/lib/";
        #env.QT_PLUGIN_PATH=${qt5.qtbase}/${qt5.qtbase.qtPluginPrefix}
        scripts.cleanup.exec = ''cd $DEVENV_ROOT && rm -rf .coverage .devenv/ .mypy_cache/ .pytest_cache/ .venv/ .pre-commit-config.yaml result'';
        processes.rebuild.exec = ''
          cd $DEVENV_ROOT
          ${pkgs.fd}/bin/fd . | ${pkgs.entr}/bin/entr -s 'nix build'
        '';
        scripts.greetthecow.exec = ''echo $(mycli $GREETING) cow!'';
        scripts.cowgreet.exec = ''cowsay $GREETING'';
        packages = [ pkgs.cowsay ];
        enterShell = ''
          greetthecow
          cowgreet
        '';
      };
    };
  };
}
