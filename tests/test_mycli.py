from click.testing import CliRunner

from mycli import cli

TESTSTRING = "Hello World!"


def test_echo():
    runner = CliRunner()

    result = runner.invoke(cli, ["cli", TESTSTRING])
    assert result.exit_code == 0
    assert result.output == f"{TESTSTRING}\n"


def test_echo_verbose():
    runner = CliRunner()

    result = runner.invoke(cli, ["cli", TESTSTRING, "-v"])
    assert result.exit_code == 0
    assert result.output == f"Now echoing input...\n{TESTSTRING}\n"
