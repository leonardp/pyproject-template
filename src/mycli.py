"""
Simple program that echoes the input it is given
"""

import importlib.metadata

import click
from trogon import tui

__version__ = importlib.metadata.version("mycli")


@tui()
@click.command()
@click.version_option(__version__)
@click.option("-v", "--verbose", is_flag=True, help="Enables verbose mode")
@click.argument("mystring", type=click.STRING)
def cli(mystring, verbose):
    """
    this is documentation specific to this function
    """
    if verbose:
        click.echo("Now echoing input...")
    click.echo(mystring)
