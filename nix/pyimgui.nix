{ lib
, fetchFromGitHub
, buildPythonPackage
# build-system
#, setuptools-scm

# dependencies
#, attrs
#, pluggy
#, py
, setuptools
, imgui
#, six

}:

buildPythonPackage rec {
  pname = "pytest";
  version = "2.0.0";
  format = "setuptools";

  src = fetchFromGitHub {
    inherit pname version;
    hash = "sha256-z3Q23FnYaVNG/NOrKW3kZCXsqwDWQJbOvnn7Ueyy65M=";
  };

  #postPatch = ''
  #  # don't test bash builtins
  #  rm testing/test_argcomplete.py
  #'';

  #nativeBuildInputs = [
  #  setuptools-scm
  #];

  propagatedBuildInputs = [
    #attrs
    #py
    setuptools
    imgui
    #six
    #pluggy
  ];

  #nativeCheckInputs = [
  #  pytest
  #];

}
