{ lib
, buildPythonPackage
, pkgs
, fetchFromGitHub
, pkg-config
, cmake
#, cython
, libX11
, libXrandr
, libXinerama
, libXcursor
, libXi
#, libXext
, glfw
#, glew
}:
buildPythonPackage rec {
  pname = "dearpygui";
  version = "1.10.1";

  src = fetchFromGitHub {
    owner = "hoffstadt";
    repo = "DearPyGui";
    rev = "6541ac7ed914163e865cb4eefd909a7857758792";
    hash = "sha256-dzysA0z3NfyrdhaCs/WRdJJvLgsb7M5e8i50ajbeM6g=";
    fetchSubmodules = true;
    deepClone = true;
  };

  nativeBuildInputs = [
    pkg-config
    cmake
  ];

  buildInputs = [
    libX11.dev
    libXrandr.dev
    libXinerama.dev
    libXcursor.dev
    libXi.dev
    #libXext
    #glfw
    #glew
  ];

  #propagatedBuildInputs = [
  #  glfw
  #  #cython
  #];

  #doCheck = true;
  dontUseSetuptoolsCheck = true;

  pythonImportsCheck = [
    "dearpygui"
  ];
}
