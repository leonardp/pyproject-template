stdlib title
------------

Here's a second file which could provide some detail on a particular module,
class, or concept.

It can include specific function docs here:

.. autofunction:: math.log
