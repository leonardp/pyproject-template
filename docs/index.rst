Welcome to the Example docs!

.. toctree::
        :maxdepth: 1

        app
        module
        extra
        pytest
